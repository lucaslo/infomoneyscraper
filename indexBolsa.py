import requests
import bs4
import json
import re
import openpyxl
import os
from pathlib import Path
from datetime import datetime


class indexBolsa(object):
    def __init__(self, indexName):
        self.indexName=indexName
        self.setRes()
        
        self.soup = bs4.BeautifulSoup(self.res.text, 'html.parser')
        self.dateTime=datetime.now()
        self.volum = self.extractInfo('.volume > p:nth-child(1)')
        self.title = self.extractInfo(
            'body > div.fill-lightgray.border-b > div > div.row > div.col-12.col-lg-8.order-2.order-lg-1 > div > div.sub-header > p')
        self.percentage = self.extractInfo(
            'body > div.fill-lightgray.border-b > div > div.row > div.col-12.col-lg-8.order-2.order-lg-1 > div > div.line-info > div.percentage > p')
        self.minimum = self.extractInfo(
            'body > div.fill-lightgray.border-b > div > div.row > div.col-12.col-lg-8.order-2.order-lg-1 > div > div.line-info > div.minimo > p')
        self.maximum = self.extractInfo(
            'body > div.fill-lightgray.border-b > div > div.row > div.col-12.col-lg-8.order-2.order-lg-1 > div > div.line-info > div.maximo > p')
        self.workBook=None
        self.createJson()
    
    def setRes(self):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'}
        self.res = requests.get(
            'https://www.infomoney.com.br/%s' % self.indexName, headers=headers)
        self.res.raise_for_status()

    def extractInfo(self, cssSelector):
        info = self.soup.select(cssSelector)
        if(len(info) != 1):
            raise Exception("Something went wrong with selector %s" %
                            (cssSelector))
        else:
            pureText = info[0].string
            pureText=re.sub(r'[\'()\n]','',pureText)
            pureText=pureText.strip()
            return '{0}'.format(pureText)

    def createWorkbook(self):
        wb = openpyxl.Workbook() 
        currentSheet =   wb.active
        currentSheet['A1'].value='Name'
        currentSheet['B1'].value='Volum'
        currentSheet['C1'].value='Max'
        currentSheet['D1'].value='Min'
        currentSheet['E1'].value='Percentage'
        currentSheet['F1'].value='Date'
        wb.save(self.workBookName)
        self.workBook = wb

    def getWorkbook(self):
        wb = openpyxl.load_workbook(self.workBookName) 
        self.workBook=wb

    def returnWorkbook(self,excelFileName):
        self.workBookName = excelFileName
        if(self.workBook == None):
            if(not os.path.exists(self.workBookName)):
                self.createWorkbook()
            else: 
                self.getWorkbook()
        return self.workBook
    
    def getDictionary(self):
        return{'title': self.title,
               'vol': self.volum,
               'max':self.maximum,
               'min':self.minimum,
               'percentage':self.percentage}

    def __str__(self):
        print("\n")
        return("Title: {}\nVol: {}\nMax: {}\nMin:{}\nPercentage:{}".format(self.title,self.volum,self.maximum,self.minimum,self.percentage))

    def createJson(self):
        self.json=json.dumps(self.getDictionary(),indent=4)
        
    def writeJson(self):
        relativeJsonPath="./json"
        jsonFolders=Path(relativeJsonPath)
        pastQuotations= 'pastQuotations'
        if(not jsonFolders.exists()):
            os.mkdir(relativeJsonPath)
        jsonFilePath=Path(relativeJsonPath+"/"+self.indexName+".json")
        jsonFile = open(jsonFilePath,"w+")
        jsonFileContent = "".join(jsonFile.readlines())
        if(jsonFileContent==""):
            jsonFileContent=str('{}')
        jsonFileContent = json.loads(jsonFileContent)
        if(not(pastQuotations in jsonFileContent)):
            jsonFileContent[pastQuotations]=[]
        jsonFileContent[pastQuotations].append(str(self.json))
        jsonFile.truncate()
        jsonFile.write(str(jsonFileContent))
        jsonFile.close()

    def writeToExcel(self,excelFileName):
        workBook = self.returnWorkbook(excelFileName)
        currentSheet = workBook.active
        newRow = currentSheet.max_row+1
        rowToWrite = [self.title,self.volum,self.maximum,self.minimum,self.percentage,self.dateTime]
        for col in range(1,len(rowToWrite)+1):
            currentSheet.cell(row=newRow, column=col).value=rowToWrite[col-1]
        workBook.save(self.workBookName)
