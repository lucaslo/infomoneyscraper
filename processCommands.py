import requests
import bs4
from indexBolsa import indexBolsa
import sys

def getAllIndexes():
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36'}
    res = requests.get(
        'https://www.infomoney.com.br/ibovespa', headers=headers)
    res.raise_for_status()
    soup = bs4.BeautifulSoup(res.text, 'html.parser')
    allIndexes = soup.select(' td:nth-child(1) > a')
    return [currentIndex.text for currentIndex in allIndexes]


def checkIfCommandIsValid(command, commandType):
    validCommands = {"input": ["-all", "-list"], "output": ["-print","-excel"]}
    return any([command == commandFromList for commandFromList in validCommands[commandType]])

def getExcelName():
    for argument in sys.argv:
        if argument.endswith(".xlsx"):
            return argument
    return "ibov.xlsx"

def processCommands():
    if(len(sys.argv) == 1):
        raise Exception("No argument passed, running as -all")
    else:
        # commands in arguments starts with -
        commands = [command for command in sys.argv if command[0] == "-"]
        if(len(commands) > 2):
            raise Exception("Should have only two commands!")
        elif(len(commands) == 2):
            if(checkIfCommandIsValid(commands[0], "input")):
                scriptInput = commands[0]
            else:
                raise Exception("No valid Input Command")
            if(checkIfCommandIsValid(commands[1], "output")):
                scriptOutput = commands[1]
            else:
                raise Exception("No valid Output Command")
            firstArgumentPos=sys.argv.index(scriptInput)+1
            lastArgumentPos=sys.argv.index(scriptOutput)
            arguments = sys.argv[firstArgumentPos:lastArgumentPos]
        else:
            raise Exception("Invalid amount of arguments")
    return {"input":scriptInput,"output":scriptOutput,"arguments":arguments}