Get data from infomoney(https://www.infomoney.com.br/) related to Ibovespa, the benchmark index of about 60 stocks that are traded on the B3 , storing them in different ways (json, excel, and sql)

Input modes: 
    -
* -all  : Gets data from all stocks in  Ibovespa (No additional argument needed)
* -list : Gets the listed stockes 

Output:

*  -print: Prints the data in the terminal (No additional argument needed)
*  -excel: Writes to an Excel, workbook name needed

Examples:

Print vale3 and petr4 stocks in the terminal console

`python3 ibovScraper.py -list petr4 vale3 -print`

Write all Indexes to an excel file

` python3 ibovScraper.py -all -excel ibov.xlsx`