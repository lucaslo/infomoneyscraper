import socketio
from indexBolsa import indexBolsa
from processCommands import processCommands, getAllIndexes, getExcelName
import requests

botname = 'ibovBot'
sio = socketio.Client() 

def connectApi():
    response = requests.post("http://localhost:3000/users/auth", data={'username':botname})
    response.raise_for_status()
    response_Json = response.json()
    return response_Json['token']['user']

def connectSocket():
    sio.connect('http://localhost:8080',headers= {'botAccount': botAccount['username']})



try:
    indexesUrl = getAllIndexes()
    botAccount = connectApi()
    connectSocket()
    @sio.on('message')
    def on_message(message):
        print(message)
        if '@'+botname in message["text"]:
            if 'help' in message["text"]:
                sio.emit("message", "Hey @{} it seems like you need help".format(message["from"]))
                sio.emit("message", "I can List all indexes from Ibovespa, and once you pick one, give specific information about it")
            if 'list' in message["text"]:
                allIndexes=""
                for index in indexesUrl:
                    allIndexes = allIndexes + " " + index
                sio.emit("message", "Ok @{}, here is a list of all the indexes {}".format(message["from"],allIndexes))
            else:
                wordByWord= message["text"].split(" ")
                for word in wordByWord:
                    if word in indexesUrl:
                        newIndex=indexBolsa(word)
                        sio.emit("message", str(newIndex))
    sio.wait()
except:
    print("Something went wrong")
    sio.disconnect()



