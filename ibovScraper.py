import requests
import bs4
from indexBolsa import indexBolsa
import sys
from processCommands import processCommands, getAllIndexes, getExcelName

indexesUrl = [] 
objIndex = []
commands = processCommands()

if(commands["input"] == "-all"):
    indexesUrl = getAllIndexes()
elif(commands["input"]  == "-list"):
    #input arguments are between the first -command and second -command
    for indexes in commands["arguments"] :
        indexesUrl.append(indexes)



for url in indexesUrl:
    try:
        newIndex = indexBolsa(url)
    except Exception as err:
        newIndex = False
        print(err)
    if(newIndex):
        if(commands["output"] == "-print"):
            print(newIndex)
        if(commands["output"]=="-excel"):
            newIndex.writeToExcel(getExcelName())
        objIndex.append(newIndex)

print('\n\n')
